# Retency on Gitlab Pages

Project to publish https://framagit.org/Mindiell/retency on Gitlab Pages.

Fork it on Gitlab. You're done, GitlabCI will download the latest version of the retency page and publish it on Gitlab Pages.

## LICENSE

This work is published under the terms of the WTPL (see [LICENSE](LICENSE) file) while Retency is published under the terms of the [AGPLv3](https://framagit.org/luc/retency/blob/master/LICENSE).

## Code of Conduct

See [Fiat Tux code of conduct](https://framagit.org/fiat-tux/code-of-conduct).
